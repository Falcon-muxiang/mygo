package main

import (
	"fmt"
	"regexp"
)

func isAllNumber(str string) bool {
	reg := regexp.MustCompile(`^\d+$`)
	return reg.MatchString(str)
}

func main() {
	fmt.Println("123456")
}
