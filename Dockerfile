FROM golang:1.14.4-alpine3.12
RUN mkdir /src

ADD . /src
ENV GOPROXY="https://goproxy.io"
RUN cd /src && ls && go build -o mygo main.go  && chmod +x mygo


FROM alpine:3.12
RUN mkdir /app
COPY --from=0 /src/mygo /app
ENTRYPOINT  ["/app/mygo"]